package com.xebia.training.zeverbot;

import org.junit.Test;

import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

/**
 * @author iwein
 */
public class SentenceExtractorTest {

    private final SentenceExtractor extractor = new SentenceExtractor();

    @Test
    public void shouldExtractSingleSentence() throws Exception {
        String sentence = "This is a sentence.";
        String html = "<p>" + sentence + "</p>";
        List<String> sentences = extractor.extractSentences(html);
        assertThat(sentences.size(), is(1));
        assertThat(sentences.get(0), is(sentence));
    }

    @Test
    public void shouldSplitSentence() throws Exception {
        String sentences = "This is a sentence. And this is another one.";
        String html = "<p>" + sentences + "</p>";
        List<String> extracted = extractor.extractSentences(html);
        assertThat(extracted.size(), is(2));
        assertThat(extracted.get(0) + " " + extracted.get(1), is(sentences));
    }
}
