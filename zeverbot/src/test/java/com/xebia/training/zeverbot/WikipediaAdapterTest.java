package com.xebia.training.zeverbot;
import com.xebia.training.zeverbot.WikipediaAdapter;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.io.*;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItems;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author iwein
 */
public class WikipediaAdapterTest {

    private WikipediaAdapter wikipediaAdapter;

    private String resultForBeard;
    private RestTemplate restTemplate;

    @Before
    public void initialize() throws IOException {
        StringWriter writer = new StringWriter();
        BufferedReader reader = new BufferedReader(new InputStreamReader(new ClassPathResource("result-for-beard.html").getInputStream()));
        String line;
        while ((line = reader.readLine())!=null){
            writer.write(line);
            writer.write('\n');
        }
        writer.close();
        resultForBeard = writer.toString();

        restTemplate = mock(RestTemplate.class);
        wikipediaAdapter = new WikipediaAdapter(restTemplate);
    }

    @Test
    public void shouldFindBeardSentences() throws Exception {
        when(restTemplate.getForObject("http://en.wikipedia.org/w/index.php?search={query}", String.class, "beard"))
                .thenReturn(resultForBeard);

        List<String> beardSentences = wikipediaAdapter.getSentencesFor("beard");
//        System.out.println(StringUtils.collectionToDelimitedString(beardSentences, "\n"));
        assertThat(beardSentences, hasItems(containsString("beard")));
        assertThat(beardSentences, hasItems(matching("^[a-zA-Z ,;-]*\\.$")));
    }

    private Matcher<String> matching(final String regex) {
        return new BaseMatcher<String>() {
            @Override
            public boolean matches(Object item) {
                return item instanceof String && ((String) item).matches(regex);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("A String matching /").appendText(regex).appendText("/.");
            }
        };
    }

}
