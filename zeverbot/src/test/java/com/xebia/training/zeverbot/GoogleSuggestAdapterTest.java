package com.xebia.training.zeverbot;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.List;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.client.RestTemplate;

public class GoogleSuggestAdapterTest {

    private GoogleSuggestAdapter googleSuggestAdapter;

    private Source resultForMicrosoft;
    private RestTemplate restTemplate;

    @Before
    public void initialize() throws IOException {
        resultForMicrosoft = new StreamSource(new InputStreamReader(new ClassPathResource("google-suggest-microsoft.xml").getInputStream()));
        restTemplate = mock(RestTemplate.class);
        googleSuggestAdapter = new GoogleSuggestAdapter(restTemplate);
    }
    @Test
    public void shouldResultInSuggestions() throws Exception {
        /*
    	when(restTemplate.getForObject("http://google.com/complete/search?q={query}&output=toolbar", Source.class, "microsoft"))
                .thenReturn(resultForMicrosoft);

        List<String> suggestionsForMicrosoft = googleSuggestAdapter.explode("beard");
        assertThat(suggestionsForMicrosoft, hasItems("microsoft"));
    */
    	GoogleSuggestAdapter gsa = new GoogleSuggestAdapter(new RestTemplate());
    	List<String> explode = gsa.explode("microsoft");
    	assertThat(explode, is(notNullValue()));
    	assertThat(explode.isEmpty(), is(false));
		for( String l : explode) {
    		System.out.println(l);
    	}
    }

}