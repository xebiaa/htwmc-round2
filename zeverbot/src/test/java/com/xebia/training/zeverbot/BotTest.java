package com.xebia.training.zeverbot;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.mockito.Mockito;

/**
 * @author iwein
 */
public class BotTest {

    private final Bot bot = new Bot();

    @Test
    public void shouldRespondToGreeting() throws Exception {
        List<String> acceptedGreetings = Arrays.asList("hallo", "hello", "hoi", "goedemorgen",
                "goedemiddag", "goedenavond");
        for (String greeting: acceptedGreetings) {
            assertThat(bot.respondTo(greeting), is("Hoi"));            
        }
    }
    
    @Test
    public void shouldIgnoreCase() {
        assertThat(bot.respondTo("HELLO"), is("Hoi"));
    }
    
    @Test
    public void shouldRespondToSomethingItDoesntUnderstand() {
        assertThat(bot.respondTo("flippety floppety floop"), is("I do not understand!"));
    }
    

    @Test
    public void shouldNotRepeatItself() {
        String firstReply = bot.respondTo("Hello");
        String secondReply = bot.respondTo("Hello");
        assertThat(firstReply, is(not(secondReply)));
        
        firstReply = bot.respondTo("Shoop da whoop");
        secondReply = bot.respondTo("Shoop da whoop");
        assertThat(firstReply, is(not(secondReply)));
    }
    
    @Test
    public void shouldRespondWithTheCurrentTime() {
    	TimeProvider mockTimeProvider = Mockito.mock(TimeProvider.class);
    	Mockito.when(mockTimeProvider.getCurrentDateTime()).thenReturn(new Date(2011, 7,4,12,0,0));
    	
        List<String> acceptedTimeQuestions = Arrays.asList(
                "what time is it?", "Hoe laat is het?");
        for (String timeQuestion: acceptedTimeQuestions) {
            String expectedAnswer = "The time is 12:00";
            bot.setTimeProvider(mockTimeProvider);
            assertThat(bot.respondTo(timeQuestion), is(expectedAnswer));
        }
        
    }
}
