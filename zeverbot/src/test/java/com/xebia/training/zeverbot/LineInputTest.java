package com.xebia.training.zeverbot;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

import java.io.StringReader;
import java.util.Iterator;

import org.junit.Test;


public class LineInputTest {

    @Test
    public void readsLineInputCorrectly() {
        String rawInput = String.format("This is a story%nabout%ninput.%n");
        StringReader str = new StringReader(rawInput);

        LineInput input = new LineInput(str);
        Iterator<String> iter = input.iterator();
        assertThat(iter, is(not(nullValue())));
        assertThat(iter.hasNext(), is(true));
        assertThat(iter.next(), is("This is a story"));
        assertThat(iter.hasNext(), is(true));
        assertThat(iter.next(), is("about"));
        assertThat(iter.hasNext(), is(true));
        assertThat(iter.next(), is("input."));
        assertThat(iter.hasNext(), is(false));
    }
}
