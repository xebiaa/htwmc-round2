package com.xebia.training.zeverbot;

import java.util.List;

import javax.xml.transform.Source;

import org.springframework.web.client.RestTemplate;
import org.springframework.xml.xpath.Jaxp13XPathTemplate;
import org.springframework.xml.xpath.NodeMapper;
import org.springframework.xml.xpath.XPathOperations;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class GoogleSuggestAdapter {
    private final RestTemplate restTemplate;
    private final XPathOperations xpathTemplate;

    public GoogleSuggestAdapter(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
		this.xpathTemplate = new Jaxp13XPathTemplate();
    }

    public List<String> explode(String query) {
    	
        Source result = restTemplate.
                getForObject("http://google.com/complete/search?q={query}&output=toolbar",
                        Source.class,
                        query);
        List<String> suggestions = xpathTemplate.evaluate("//suggestion", result, new NodeMapper<String>() {
            public String mapNode(Node node, int i) throws DOMException {
                Element suggestion = (Element) node;
                return suggestion.getAttribute("data");
            }
        });
        return suggestions;
    }
}
