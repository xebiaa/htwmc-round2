package com.xebia.training.zeverbot;

import java.text.SimpleDateFormat;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author iwein
 */
public class Bot {
	
	private TimeProvider timeProvider = new DefaultTimeProvider();

    private static final SimpleDateFormat TIME_FORMATTING = new SimpleDateFormat("HH:mm");

    static List<String> acceptedGreetings = Arrays.asList("hallo", "hello",
            "hoi", "goedemorgen", "goedemiddag", "goedenavond");
    
    static List<String> acceptedTimeQuestions = Arrays.asList(
        "what time is it?", "hoe laat is het?"
            );

    Map<String, String> lastReplies = new HashMap<String, String>();

    public String respondTo(String input) {
        input = input.toLowerCase();
        String answer;

        if (lastReplies.containsKey(input)) {
            return "Do you really want me to repeat myself?";
        }
        
        if (acceptedGreetings.contains(input)) {
            answer = "Hoi";
        } 
        else if (acceptedTimeQuestions.contains(input)) {
            String time = TIME_FORMATTING.format(timeProvider.getCurrentDateTime());
            answer = "The time is " + time;
        }
        else {
            answer = "I do not understand!";
        }

        lastReplies.put(input, answer);
        return answer;
    }

	public void setTimeProvider(TimeProvider timeProvider) {
		this.timeProvider = timeProvider;
	}
}
