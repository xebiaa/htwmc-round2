package com.xebia.training.zeverbot;

/**
 * Operates a zeverbot on stdin and stdout.
 * @author barend
 */
public class ConsoleInterface {

    private Bot bot = new Bot();

    public static void main(String[] args) {
        new ConsoleInterface().inputLoop();
    }

    private void inputLoop() {
        LineInput console = new LineInput();
        System.out.println("Type \\exit to quit the application");
        prompt();
        for (String input : console) {
            if ("\\exit".equals(input)) {
                System.out.println("Doei!");
                break;
            }
            System.out.println(bot.respondTo(input));
            prompt();
        }
    }

    private void prompt() {
        System.out.println();
        System.out.print("zeverbot> ");
    }
}
