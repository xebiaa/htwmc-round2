package com.xebia.training.zeverbot;

import java.util.Date;

public interface TimeProvider {
	public Date getCurrentDateTime();
}
