package com.xebia.training.zeverbot;

import java.util.Date;

public class DefaultTimeProvider implements TimeProvider {

	@Override
	public Date getCurrentDateTime() {
		return new Date();
	}

}
