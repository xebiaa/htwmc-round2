package com.xebia.training.zeverbot;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Iterator;
import org.apache.commons.io.LineIterator;

/**
 * Abstracts line input as an {@code Iterable<String>}.
 * @author barend
 */
public class LineInput implements Iterable<String> {

    private LineIterator iterator;

    /**
     * Input will be obtained from {@code System.in}.
     */
    public LineInput() {
        this(System.in);
    }

    /**
     * Line input will be obtained from the given stream.
     * @param input
     */
    public LineInput(InputStream input) {
        this(new BufferedReader(new InputStreamReader(input)));
    }

    /**
     * Input will be obtained from the given reader.
     * @param reader
     */
    public LineInput(Reader reader) {
        iterator = new LineIterator(reader);
    }

    @Override public Iterator<String> iterator() {
        return iterator;
    }
}
