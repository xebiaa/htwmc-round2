package com.xebia.training.zeverbot;

import net.htmlparser.jericho.HTMLElementName;
import net.htmlparser.jericho.Source;
import net.htmlparser.jericho.StartTag;
import net.htmlparser.jericho.TextExtractor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;

import static java.util.Arrays.asList;

public class SentenceExtractor {
    List<String> extractSentences(String sourceString) {
        Source source = createSource(sourceString);
        return splitStringOnSentenceTerminator(
                new ParagraphTextExtractor(source).toString());
    }

    private static class ParagraphTextExtractor extends TextExtractor {

        public ParagraphTextExtractor(Source source) {
            super(source);
        }

        @Override
        public boolean excludeElement(StartTag startTag) {
            return isNotContentParagraphAncestor(startTag) || isIrrelevantContent(startTag);
        }

        private boolean isNotContentParagraphAncestor(StartTag startTag) {
            String id = startTag.getAttributeValue("id");
            return startTag.getName().equals(HTMLElementName.DIV) &&
                    !(
                            "content".equalsIgnoreCase(id)
                                    || "bodyContent".equalsIgnoreCase(id)
                    );
        }

        private boolean isIrrelevantContent(StartTag startTag) {
            return (
                    startTag.getName().equals(HTMLElementName.H1) ||
                    startTag.getName().equals(HTMLElementName.TITLE)
            );
        }
    }

    private static final String SENTENCE_TERMINATOR = "(?<=\\.)( |\\[\\d*\\])";

    private List<String> splitStringOnSentenceTerminator(String stripped) {
        return asList(stripped.split(SENTENCE_TERMINATOR));
    }

    private Source createSource(String sourceString) {
        BufferedReader reader = new BufferedReader(new StringReader(sourceString));
        Source source = null;
        try {
            source = new Source(reader);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return source;
    }
}