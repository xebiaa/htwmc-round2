Instructions for the labs
=========================

Follow these instructions to get started with the labs, whenever you run into trouble with git or the tooling ask the instructor.

Set up your environment
-----------------------
Your environment should be preinstalled with Java, Maven, Eclipse and a Git client ([MSysgit](http://code.google.com/p/msysgit/)). You can verify this by running Git Bash and running `java -version` from the bash shell. You can run Eclipse too to verify that it is installed correctly.

Now take the following steps to get up and running:

1. Get a git account and follow [the setup guide](http://help.github.com/win-set-up-git/) from the step "Set up SSH keys"
   (this assumes git and tooling are pre-installed)
2. Ask the instructor to give you push access to the github repository
3. Clone the project `git clone git@github.com:xebia/htwmc-round2.git`
4. Start your IDE and import the project
5. Create a local branch for development and check it out `git checkout -b <my-branch>`
6. Start working

Whenever you're ready to commit something you can use `git add` to add what you want to commit to the index and `git commit` to commit locally. We'll discuss pushing in a later lab. If you get in trouble, ask the instructor or refer to a git cheat sheet, such as this one: 
![git overview](http://osteele.com/images/2008/git-transport.png)

You can run `git help <command>` to open the standard documentation for any command.

